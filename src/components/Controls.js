import React from 'react';
import { Button, Tooltip, Icon } from 'antd';
import { connect } from 'react-redux';

import {
  controlAction,
  makeCall,
  moveToLocation,
  openTicket,
  makeInfo,
  makeWarning,
  makeFailure,
  makeAchievement,
} from '../actions';

class Controls extends React.Component {
  state = {
    demoInterval: null,
  };

  triggerDemoAction = () => {
    const allActions = [
      this.props.controlAction,
      this.props.makeCall,
      this.props.moveToLocation,
      this.props.openTicket,
      this.props.makeInfo,
      this.props.makeWarning,
      this.props.makeFailure,
      this.props.makeAchievement,
    ];
    return allActions[Math.round(Math.random() * (allActions.length - 1))]();
  };

  toggleDemoMode = () => {
    if (this.state.demoInterval) {
      clearInterval(this.state.demoInterval);
      this.setState({ demoInterval: null });
    } else {
      this.triggerDemoAction();
      const demoInterval = setInterval(() => {
        this.triggerDemoAction();
      }, 3000);
      this.setState({ demoInterval });
    }
  };

  render() {
    return (
      <div>
        <div style={{ display: 'flex', flexDirection: 'column', width: '40px' }}>
          <Tooltip title="Make adjustment" placement="right">
            <Button onClick={this.props.controlAction} color="#a0d911" icon="tool" size="large" />
          </Tooltip>
          <br />

          <Tooltip title="Call someone" placement="right">
            <Button onClick={this.props.makeCall} icon="phone" size="large" />
          </Tooltip>
          <br />

          <Tooltip title="Open ticket" placement="right">
            <Button onClick={this.props.openTicket} icon="tag" size="large" />
          </Tooltip>
          <br />

          <Tooltip title="Go to location" placement="right">
            <Button onClick={this.props.moveToLocation} icon="rocket" size="large" />
          </Tooltip>
          <br />

          <Tooltip title="System Info" placement="right">
            <Button onClick={this.props.makeInfo} icon="info" size="large" />
          </Tooltip>
          <br />

          <Tooltip title="Alert Warning" placement="right">
            <Button onClick={this.props.makeWarning} icon="warning" size="large" />
          </Tooltip>
          <br />

          <Tooltip title="Ohh something is wrong" placement="right">
            <Button onClick={this.props.makeFailure} icon="fire" size="large" />
          </Tooltip>
          <br />

          <Tooltip title="Reward operator" placement="right">
            <Button onClick={this.props.makeAchievement} icon="trophy" size="large" />
          </Tooltip>
          <br />

          <Tooltip title="Start/Stop Demo mode" placement="right">
            <Button
              type="dashed"
              onClick={this.toggleDemoMode}
              style={{ justifyContent: 'center', alignItems: 'center', display: 'flex' }}
            >
              <Icon
                type={this.state.demoInterval ? 'sync' : 'caret-right'}
                spin={!!this.state.demoInterval}
                size="large"
              />
            </Button>
          </Tooltip>
          <br />
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  controlAction: item => dispatch(controlAction(item)),
  makeCall: item => dispatch(makeCall(item)),
  moveToLocation: item => dispatch(moveToLocation(item)),
  openTicket: item => dispatch(openTicket(item)),
  makeInfo: item => dispatch(makeInfo(item)),
  makeWarning: item => dispatch(makeWarning(item)),
  makeFailure: item => dispatch(makeFailure(item)),
  makeAchievement: item => dispatch(makeAchievement(item)),
});

export default connect(
  null,
  mapDispatchToProps,
)(Controls);
