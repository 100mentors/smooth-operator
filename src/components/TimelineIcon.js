import React from 'react';
import { Icon, Row, Tooltip } from 'antd';

const TimelineIcon = ({ type, color, icon, title }) => (
  <Tooltip title={title}>
    <Row
      justify="center"
      align="middle"
      style={{
        borderColor: color,
        backgroundColor: '#fff',
        borderRadius: '50%',
        borderWidth: '2px',
        borderStyle: type === 'todo' ? 'none' : 'solid',
        padding: '5px',
      }}
    >
      <Icon type={icon} style={{ color }} theme="twoTone" twoToneColor={color} />
    </Row>
  </Tooltip>
);

export default TimelineIcon;
