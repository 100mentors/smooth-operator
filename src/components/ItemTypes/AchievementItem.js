import React from 'react';
import { Icon, Popover, Card } from 'antd';
import './AchievementItem.css';

const AchievementItem = ({ title, description, image, icon, color }) => {
  const content = (
    <Card style={{ width: 240 }} cover={<img alt="Achievement" src={image} />}>
      <Card.Meta title={title} description={description} />
    </Card>
  );

  return (
    <Popover style={{ padding: 0 }} content={content} title="Achievement unlocked!" placement="left">
      <div>
        <Icon type={icon} style={{ color, fontSize: '28px' }} theme="twoTone" />
      </div>
    </Popover>
  );
};

export default AchievementItem;
