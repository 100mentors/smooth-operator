import React from 'react';
import { Row, Col, Icon, Checkbox } from 'antd';
import styled from 'styled-components';
import moment from 'moment';

const Description = styled.div`
  transition: all 150ms ease-in;
  opacity: 0;
  max-height: 0;
  font-size: 12px;
`;

const Separator = styled.div`
  width: 100%;
  border-bottom: 2px dotted #e8e8e8;
  text-align: right;
  left: -50%;
  position: absolute;
  top: -10px;
`;

const Wrapper = styled.div`
  cursor: pointer;
  transition: all 150ms ease-in;
  border-width: 1px;
  border-style: solid;
  border-color: white;
  padding: 5px;
  margin-left: 10px;

  &:hover {
    border-color: ${props => props.color};
    background-color: ${props => props.color}16;

    ${Description} {
      opacity: 1;
      max-height: 150px;
    }
  }
`;

const TodoItem = ({ color, title, subtype }) => (
  <div>
    <Wrapper color={color}>
      {subtype === 'default' && (
        <div>
          Todo: &nbsp;
          {title}
        </div>
      )}
      {subtype === 'separator' && <Separator>Now</Separator>}
    </Wrapper>
  </div>
);

export default TodoItem;
