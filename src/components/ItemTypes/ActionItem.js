import React from 'react';
import { Row, Col, Icon } from 'antd';
import styled from 'styled-components';
import moment from 'moment';

const Description = styled.div`
  transition: all 150ms ease-in;
  opacity: 0;
  max-height: 0;
  font-size: 12px;
`;

const Wrapper = styled.div`
  cursor: pointer;
  transition: all 150ms ease-in;
  border-width: 1px;
  border-style: solid;
  border-color: white;
  padding: 5px;
  margin-left: 10px;

  &:hover {
    border-color: ${props => props.color};
    background-color: ${props => props.color}16;

    ${Description} {
      opacity: 1;
      max-height: 150px;
    }
  }
`;

const ActionItem = ({ color, title, description, timestamp }) => (
  <div>
    <Row style={{ paddingLeft: '16px', marginBottom: '2px', color: '#BDBDBD', fontSize: '12px', fontWeight: '500' }}>
      <Icon type="clock-circle" style={{ marginRight: '2px' }} />
      {moment(timestamp).from(moment(), true)}
    </Row>
    <Wrapper color={color}>
      <Col>
        <div>{title}</div>
        <Description>{description}</Description>
      </Col>
    </Wrapper>
  </div>
);

export default ActionItem;
