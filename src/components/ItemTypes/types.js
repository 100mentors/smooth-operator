const types = {
  event: {
    subtype: {
      info: {
        hasCustomIcon: false,
        color: '#9e9e9e',
      },
      warning: {
        hasCustomIcon: true,
        icon: 'warning',
        title: 'Warning',
        color: '#ffc069',
      },
      failure: {
        hasCustomIcon: true,
        icon: 'fire',
        title: 'Failure',
        color: '#ff4d4f',
      },
      start: {
        hasCustomIcon: true,
        icon: 'environment',
        title: 'Good evening!',
        color: 'green',
      },
    },
  },
  achievement: {
    position: 'left',
    style: {
      left: '-55px',
      width: 'fit-content',
    },
    subtype: {
      nightOwl: {
        icon: 'trophy',
        color: '#1890ff',
      },
    },
  },
  action: {
    subtype: {
      default: {},
      call: { hasCustomIcon: true, icon: 'phone', color: '1890ff', title: 'Phone Call' },
      handover: { hasCustomIcon: true, icon: 'message', color: '#9e9e9e', title: 'Hand Over' },
      ticket: { hasCustomIcon: true, icon: 'tag', color: '#b37feb', title: 'Ticket Opened' },
      move: { hasCustomIcon: true, icon: 'rocket', color: '#36cfc9', title: 'Location changed' },
      control: { hasCustomIcon: true, icon: 'tool', color: '#a0d911', title: 'Control Adjustment' },
    },
  },
  todo: {
    subtype: {
      default: {
        hasCustomIcon: true,
        icon: 'schedule',
        color: '1890ff',
      },
      separator: {
        color: '1890ff',
      },
    },
  },
};

export default types;
