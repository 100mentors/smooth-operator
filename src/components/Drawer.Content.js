import React from 'react';
import { Button, Icon, Avatar, Divider } from 'antd';
import styled from 'styled-components';
import moment from 'moment';
import colors from './colors';

const Styled = {
  Content: styled.div`
    width: 100%;
    height: 100%;
  `,
  Header: styled.div`
    display: flex;
    flex-direction: row;
    min-height: 32px;
    padding: 24px 20px;
    align-items: center;
    background-color: ${({ type }) => {
      if (type === 'warning') return '#ffc06916';
      if (type === 'failure') return '#ff4d4f16';

      return '#1890ff16';
    }};
    border-bottom: 2px solid
      ${({ type }) => {
        if (type === 'warning') return '#ffc069';
        if (type === 'failure') return '#ff4d4f';

        return '#1890ff';
      }};
  `,
  EventIcon: styled.div`
    margin-right: 24px;
    margin-bottom: -3px;
    color: ${({ type }) => {
      if (type === 'warning') return '#ffc069';
      if (type === 'failure') return '#ff4d4f';

      return '#1890ff';
    }};
  `,
  Title: styled.div`
    color: ${colors.Black};
    font-size: 16px;
    font-weight: bold;
    padding-right: 12px;
  `,
  Time: styled.div`
    color: ${colors.Grey2};
  `,
  Description: styled.div`
    color: ${colors.Grey2};
    margin-top: 5px;
    max-width: 430px;
  `,
  PeopleOfInterest: styled.div`
    width: 100%;

    span {
      margin-right: 5px;
    }
  `,
  Heading: styled.div`
    font-size: 16px;
    margin-top: 12px;
  `,
  HeadingDescription: styled.div`
    font-size: 12px;
    color: ${colors.Grey2};
    margin-bottom: 12px;
    text-transform: uppercase;
  `,
  RelevantInfo: styled.div``,
  Recommended: styled.div``,
  Wrapper: styled.div`
    padding: 20px;
  `,
  SectionSeperator: styled.div`
    width: 100%;
    height: 1px;
    content: ' ';
    padding: 27px;
  `,
  ListItem: styled.div`
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 12px 0px;
    border-bottom: 1px solid ${colors.Grey1};
  `,
  ItemContent: styled.div``,
  RightContent: styled.div`
    font-size: 12px;
    text-transform: uppercase;
  `,
  Action: styled.div``,
  ActionSub: styled.div`
    color: ${colors.Grey2};
  `,
};

const subTypeToIcon = subtype => {
  if (subtype === 'failure') return 'fire';
  if (subtype === 'warning') return 'warning';
  if (subtype === 'info') return 'info-circle';
};

const Content = ({ item }) => (
  <Styled.Content>
    <Styled.Header type={item.subtype}>
      <Styled.EventIcon type={item.subtype}>
        <Icon type={subTypeToIcon(item.subtype)} style={{ fontSize: 32 }} />
      </Styled.EventIcon>
      <div style={{ flex: '1' }}>
        <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
          <Styled.Title>{item.title}</Styled.Title>
          <Styled.Time>{moment(item.timestamp).from(moment(), true)}</Styled.Time>
        </div>
        <Styled.Description>{item.description ? item.description : 'No description available'}</Styled.Description>
      </div>
    </Styled.Header>
    <Styled.Wrapper>
      {item.RelevantInfo && (
        <React.Fragment>
          <Styled.Heading>Relevant Information</Styled.Heading>
          <Styled.RelevantInfo>{item.relevantInfo}</Styled.RelevantInfo>
        </React.Fragment>
      )}

      <Styled.Heading style={{ margin: 0 }}>Relevant People</Styled.Heading>
      <Styled.HeadingDescription>People you may need to get in contact</Styled.HeadingDescription>
      <Styled.PeopleOfInterest>
        <Avatar icon="user" shape="square" size="large" src="https://randomuser.me/api/portraits/men/43.jpg" />
        <Avatar icon="user" shape="square" size="large" src="https://randomuser.me/api/portraits/women/68.jpg" />
        <Avatar
          icon="user"
          size="large"
          shape="square"
          src="https://tinyfac.es/data/avatars/2DDDE973-40EC-4004-ABC0-73FD4CD6D042-200w.jpeg"
        />
      </Styled.PeopleOfInterest>

      <Styled.SectionSeperator />

      <Styled.Heading>Summary</Styled.Heading>
      <Styled.HeadingDescription>Report of how the incident occured</Styled.HeadingDescription>
      <p style={{ padding: 0, margin: 0 }}>
        Cupcake ipsum dolor sit amet toffee bear claw. Cupcake wafer brownie marshmallow donut bear claw jelly jujubes
        soufflé. Liquorice jelly candy canes bear claw pudding ice cream cake chocolate bar. Donut topping gummi bears
        chocolate bar ice cream donut brownie toffee. Brownie toffee carrot cake chocolate cake macaroon liquorice.
        Fruitcake chocolate bar dessert halvah chocolate pie lollipop powder.
      </p>

      <Styled.SectionSeperator />

      <Styled.Heading>Suggested Actions</Styled.Heading>
      <Styled.HeadingDescription>Check what you colleagues did in a similar situation</Styled.HeadingDescription>

      <Styled.Recommended>
        <Styled.ListItem>
          <Styled.ItemContent>
            <Styled.Action>Increase feed flow</Styled.Action>
            <Styled.ActionSub>Last used 2 days ago</Styled.ActionSub>
          </Styled.ItemContent>
          <Styled.RightContent>
            <div>89% Success Rate</div>
          </Styled.RightContent>
        </Styled.ListItem>
        <Styled.ListItem>
          <Styled.ItemContent>
            <Styled.Action>Close air value</Styled.Action>
            <Styled.ActionSub>Last used 1 days ago</Styled.ActionSub>
          </Styled.ItemContent>
          <Styled.RightContent>
            <div>87% Success Rate</div>
          </Styled.RightContent>
        </Styled.ListItem>
        <Styled.ListItem>
          <Styled.ItemContent>
            <Styled.Action>Decrease froth level</Styled.Action>
            <Styled.ActionSub>Last used 3 days ago</Styled.ActionSub>
          </Styled.ItemContent>
          <Styled.RightContent>
            <div>60% Success Rate</div>
          </Styled.RightContent>
        </Styled.ListItem>
      </Styled.Recommended>
    </Styled.Wrapper>
  </Styled.Content>
);

export default Content;
