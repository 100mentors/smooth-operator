import styled from 'styled-components';
import { Drawer } from 'antd';

export default {
  Drawer: styled(Drawer)`
    .ant-drawer-body {
      padding: 0;
    }
  `,
};
