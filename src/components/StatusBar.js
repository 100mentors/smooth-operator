import React from 'react';
import styled from 'styled-components';
import { Icon, Popover } from 'antd';

const S = {
  StatusBar: styled.div`
    display: flex;
    align-items: center;
    margin-right: 24px;
  `,
  ResponseTime: styled.div`
    margin-left: 24px;
    font-size: 16px;
  `,
  HeartRate: styled.div`
    font-size: 16px;
  `,
  HeartPopover: styled.div`
    padding: 12px;
    max-width: 150px;
  `,
  ResponsePopover: styled.div`
    padding: 12px;
    max-width: 150px;
  `,
};

const HeartPopover = () => (
  <S.HeartPopover>
    <span>Nice! Your vitals look good.</span>
  </S.HeartPopover>
);

const ResponsePopover = () => (
  <S.ResponsePopover>
    <span>You're average time for reacting to events is 4.2s</span>
  </S.ResponsePopover>
);

function randomIntFromInterval(min, max) {
  // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min);
}

class StatusBar extends React.Component {
  state = {
    heartRate: 70,
  };

  heartInterval = null;

  componentDidMount() {
    this.heartInterval = setInterval(() => {
      this.setState({ heartRate: randomIntFromInterval(60, 70) });
    }, 1000);
  }

  render() {
    return (
      <S.StatusBar>
        <Popover content={HeartPopover()} title="Heart Rate">
          <S.HeartRate>
            <Icon type="heart" theme="filled" style={{ fontSize: 18, color: 'rgb(255, 77, 79)' }} />{' '}
            {this.state.heartRate} BPM
          </S.HeartRate>
        </Popover>

        <Popover content={ResponsePopover()} title="Average Response">
          <S.ResponseTime>
            <Icon type="thunderbolt" theme="filled" style={{ fontSize: 18, color: '' }} /> 4.2s
          </S.ResponseTime>
        </Popover>
      </S.StatusBar>
    );
  }
}

export default StatusBar;
