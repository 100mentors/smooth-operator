import React from 'react';

import Styled from './Drawer.Styled';
import Content from './Drawer.Content';

const TopicDrawer = ({ visible, onClose, item }) => (
  <Styled.Drawer
    width={500}
    placement="right"
    visible={visible}
    onClose={onClose}
    closable={false}
    maskClosable
    destroyOnClose
  >
    <Content item={item} />
  </Styled.Drawer>
);

export default TopicDrawer;
