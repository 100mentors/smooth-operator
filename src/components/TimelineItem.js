import React from 'react';
import { Timeline } from 'antd';
import styled, { css } from 'styled-components';
import './TimelineItem.less';

import AchievementItem from './ItemTypes/AchievementItem';
import EventItem from './ItemTypes/EventItem';
import TodoItem from './ItemTypes/TodoItem';
import ActionItem from './ItemTypes/ActionItem';
import TimelineIcon from './TimelineIcon';
import types from './ItemTypes/types';

const Wrapper = styled(Timeline.Item)`
  ${({ type }) =>
    type === 'achievement' &&
    css`
      .ant-timeline-item-content {
        top: -10px;
        left: -60px;
        width: fit-content;
      }
    `}
`;

const TimelineItem = ({ dotted, item, ...props }) => {
  const type = types[item.type];
  const subtype = type.subtype[item.subtype];

  const classList = [];
  if (dotted || item.type === 'todo') classList.push('ant-timeline-item-last');

  return (
    <Wrapper
      {...props}
      className={classList.join(' ')}
      dot={subtype.hasCustomIcon ? <TimelineIcon type={item.type} {...subtype} /> : null}
      color={subtype.color || 'grey'}
      type={item.type}
    >
      {item.type === 'event' && <EventItem {...item} color={subtype.color} />}
      {item.type === 'achievement' && <AchievementItem {...subtype} {...item} />}
      {item.type === 'action' && <ActionItem {...item} color={subtype.color} />}
      {item.type === 'todo' && <TodoItem {...item} color={subtype.color} />}
    </Wrapper>
  );
};

export default TimelineItem;
