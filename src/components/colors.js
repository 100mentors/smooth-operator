export default {
  Black: '#3e3e3e',
  Grey1: '#e8e8e8',
  Grey2: '#c3c3c3',
  Grey3: '898989',
  Lavender: '#fbf8ff',
};
