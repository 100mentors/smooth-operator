import { combineReducers } from 'redux';
import itemHistory from './itemHistory';

const items = (state = [], action) => {
  switch (action.type) {
    case 'ADD_ITEM': {
      const [todo1, todo2, todo3, ...restItems] = state;
      return [todo1, todo2, todo3, action.item, ...restItems];
    }
    default:
      return itemHistory;
  }
};

export default combineReducers({
  items,
});
