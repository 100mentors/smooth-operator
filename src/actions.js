import uuidv1 from 'uuid/v1';
import _ from 'lodash';

import { randomName } from './data/names';
import { randomControlMessage } from './data/control_messages';
import { randomMalfunction } from './data/malfunctions';

export const addItem = item => ({
  type: 'ADD_ITEM',
  item,
});

export const controlAction = () => {
  return addItem({
    id: uuidv1(),
    type: 'action',
    subtype: 'control',
    title: randomControlMessage(),
    timestamp: new Date(),
  });
};

export const makeCall = () => {
  return addItem({
    id: uuidv1(),
    type: 'action',
    subtype: 'call',
    title: `Called ${randomName()}`,
    timestamp: new Date(),
  });
};

const locations = ['furnice', 'feeder', 'valve control', 'fuse box', 'exhaust'];

export const moveToLocation = () => {
  return addItem({
    id: uuidv1(),
    type: 'action',
    subtype: 'move',
    title: `Moved to ${locations[_.random(0, locations.length - 1)]}`,
    timestamp: new Date(),
  });
};

export const openTicket = () => {
  const id = uuidv1();
  return addItem({ id, type: 'action', subtype: 'ticket', title: `Opened ticket ${id}`, timestamp: new Date() });
};

export const makeInfo = () => {
  return addItem({
    id: uuidv1(),
    type: 'event',
    subtype: 'info',
    title: randomMalfunction(),
    timestamp: new Date(),
  });
};

export const makeWarning = () => {
  return addItem({
    id: uuidv1(),
    type: 'event',
    subtype: 'warning',
    title: randomMalfunction(),
    timestamp: new Date(),
  });
};

export const makeFailure = () => {
  return addItem({
    id: uuidv1(),
    type: 'event',
    subtype: 'failure',
    title: randomMalfunction(),
    timestamp: new Date(),
  });
};

export const makeAchievement = () => {
  const subtype = 'nightOwl';
  const title = 'Night Owl';
  const description = 'Tweaking the hot one at 3:00 AM, good one pal!';
  const image = 'https://res.cloudinary.com/w100mentors/image/upload/v1543106380/NIGHT_OWL_Lee_Romao.jpg';
  return addItem({ id: uuidv1(), type: 'achievement', subtype, title, description, image, timestamp: new Date() });
};
