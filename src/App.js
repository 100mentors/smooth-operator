import 'antd/dist/antd.css';

import React, { Component } from 'react';
import { Layout, Badge, Avatar, Row } from 'antd';
import moment from 'moment';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import rootReducer from './reducers';
import Drawer from './components/Drawer';
import TimeLine from './containers/TimeLine';
import Controls from './components/Controls';
import StatusBar from './components/StatusBar';

import Logo from './data/outotec-logo.svg';

moment.locale('en', {
  relativeTime: {
    future: 'in %s',
    past: '%s ago',
    s: 'just now',
    ss: '%ss',
    m: 'a minute',
    mm: '%dm',
    h: 'an hour',
    hh: '%dh',
    d: 'a day',
    dd: '%dd',
    M: 'a month',
    MM: '%dM',
    y: 'a year',
    yy: '%dY',
  },
});

const store = createStore(rootReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

class App extends Component {
  state = {
    user: {
      name: 'John',
      avatarUrl:
        'https://res.cloudinary.com/react-operator/image/upload/c_fill,h_400,w_400/assets/Screen_Shot_2018-11-24_at_20.07.05.png',
    },
    isDrawerVisible: false,
    drawerItem: null,
  };

  showDrawerForItem = item => {
    this.setState({
      isDrawerVisible: true,
      drawerItem: item,
    });
  };

  hideDrawer = () => {
    this.setState({
      isDrawerVisible: false,
    });
  };

  render() {
    const { isDrawerVisible, drawerItem } = this.state;

    return (
      <Provider store={store}>
        <Layout style={{ height: '100vh' }}>
          <Layout.Header style={{ backgroundColor: '#f5f4f5', borderBottom: '1px solid #e2e2e2' }}>
            <Row type="flex" justify="space-between" align="middle" style={{ height: '100%' }}>
              <div>
                <img src={Logo} style={{ height: 21 }} />
              </div>
              <div style={{ display: 'flex', alignItems: 'center' }}>
                <StatusBar />
                <Badge count={7}>
                  <Avatar shape="square" src={this.state.user.avatarUrl} />
                </Badge>
              </div>
            </Row>
          </Layout.Header>
          <Layout.Content
            style={{
              backgroundColor: '#fff',
              display: 'flex',
            }}
          >
            <div
              style={{
                padding: '30px',
                overflow: 'auto',
                flex: '1',
                display: 'flex',
                justifyContent: 'center',
                position: 'relative',
              }}
            >
              <TimeLine showDrawerForItem={this.showDrawerForItem} />
              <Drawer visible={isDrawerVisible} item={drawerItem} onClose={() => this.hideDrawer()} />
            </div>
            <div
              style={{
                padding: '10px',
                overflow: 'auto',
                justifyContent: 'center',
                position: 'relative',
                borderLeft: '1px solid #f5f4f5',
              }}
            >
              <Controls />
            </div>
          </Layout.Content>
        </Layout>
      </Provider>
    );
  }
}

export default App;
