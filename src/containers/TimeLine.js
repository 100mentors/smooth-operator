import React from 'react';
import { Timeline } from 'antd';
import { connect } from 'react-redux';

import { addItem } from '../actions';
import TimelineItem from '../components/TimelineItem';

const TimeLine = ({ items, showDrawerForItem }) => {
  return (
    <Timeline pending="Loading more..." style={{ paddingLeft: '50px', paddingTop: '50px', flex: '1' }}>
      {items.map((item, index) => (
        <TimelineItem
          key={item.id || `${item.type}_${item.subtype}_${index}`}
          item={item}
          onClick={() => (item.type === 'event' ? showDrawerForItem(item) : '')}
        />
      ))}
    </Timeline>
  );
};

const mapStateToProps = state => ({
  items: state.items,
});

const mapDispatchToProps = dispatch => ({
  addItem: item => dispatch(addItem(item)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TimeLine);
