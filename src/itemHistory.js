import moment from 'moment';

const itemHistory = [
  {
    type: 'todo',
    subtype: 'default',
    title: 'Walk around the plant',
  },
  {
    type: 'todo',
    subtype: 'default',
    title: 'Eat something, take a break',
  },
  {
    type: 'todo',
    subtype: 'separator',
  },
  {
    type: 'event',
    subtype: 'info',
    title: 'All systems normal',
    description: 'Plant operating at 98% \n No anomalies detected.',
    timestamp: moment().subtract(15, 'minutes'),
  },
  {
    type: 'action',
    subtype: 'handover',
    title: 'Handover from evening shift',
    description: 'Jane did some manual measurements on furnice A632 and reported a ticket for valve 1 maintenance.',
    timestamp: moment().subtract(17, 'minutes'),
  },
  {
    type: 'event',
    subtype: 'start',
    title: 'Shift start',
    description: 'Good evening John! How are you since yesterday?',
    timestamp: moment().subtract(32, 'minutes'),
  },
];

export default itemHistory;
