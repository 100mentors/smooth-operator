const malfunctions = [
  'ADS Inadvertent initiation of ADS',
  'CBS Loss of containment vacuum',
  'CNR Loss of condenser vacuum',
  'CWS Condenser coolant pumps trip',
  'DHR Inadvertent initiation of decay heat removal system',
  'FWS Reduction in feedwater temperature (loss of FW heating)',
  'Abnormal increase in FW flow',
  'Loss of normal feedwater flow (FW pumps trip)',
  'Feedwater system pipe break',
  'GEN Station blackout (loss of AC power)',
  'MSS Steam header break',
  'Steam generator tube failure 1',
  'Steam generator tube failure 2',
  'Major steam system piping failure within containment',
  'RCS Reactor setback fail',
  'One bank of shutdown control rods drop into the core',
  'Charging (feed) valve fails open',
  'Inadvertent operation of pressurizer heaters',
  'Uncontrolled control rod assembly withdrawal at power',
  'Fail of PZR control system',
  'Failure of main coolant pumps',
  'Reactor stepback fail',
  'Seismic event',
  'TUR Turbine spurious trip',
  'Turbine spurious runback',
  'Turbine trip with bypass valves failed closed',
];

function randomMalfunction() {
  return malfunctions[Math.round(Math.random() * (malfunctions.length - 1))];
}

export { malfunctions, randomMalfunction };
