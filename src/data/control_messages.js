const controMessages = [
  'Heaters P =15.5 MPA',
  'Spray P =15.5 MPA',
  'Relief RCS valve P=16.7 MPA',
  'Safety RCS valve P=17.05MPA',
  'Alarm low RCS pressure P=12.5MPA',
  'PCS-Trips Low pressure upper plenum P =11. MPA',
  'Low level PZR L=5%',
  'Low flow downcomer W=347Kg/s',
  'High core outlet temperature T=340ºC',
  'High reactor neutron core flux Flux=120%',
  'High log rate SUR=2dpm',
  'High pressure upper plenum P=16.4',
  'PCS-Setbacks High PZR level L=67%',
  'High steam header pressure P=4.9 MPA',
  'PCS-Stepbacks High zone flux Flux=115%',
  'ADS Low low pressure upper plenum P =9 MPA',
  'Low low level vessel L=90%',
  'High-high pressure upper plenum P=17.2MPA',
  'High-high pressure CBS P=0.019MPA',
  'CBS Alarm high CBS pressure P=0.012MPA',
  'CCS CCS starts at CBS pressure P=0.019MPA',
  'PIS Pressure set point P=5MPA',
  'GIS Pressure set point P=0.5MPA',
  'CNR Alarm low CNR vacuum P=100 mmHg',
  'Loss of CNR vacuum P=254 mmHg',
  'MSS Relief RCS valve P=5.3 MPA',
  'Safety RCS valve P=5.7 MPA',
];

function randomControlMessage() {
  return controMessages[Math.round(Math.random() * (controMessages.length - 1))];
}

export { controMessages, randomControlMessage };
